package model

data class FilmItb(
    private val users: MutableList<User> = mutableListOf(),
    private val films: MutableList<Film> = mutableListOf()
) {
    //users
    /**
     * @return a List of all users
     */
    fun getAllUsers(): List<User> {
        return users
    }

    fun getUser(user: User): User {
        return user
    }

    /**
     * @param newUser is the new user for filmItb
     */
    fun addUser(newUser: User) {
        users.add(newUser)
    }

    /**
     * @param user is the user then will be change
     * @param newUser is the new name for the user will be change
     */
    fun updateUser(user: User, newUser: User){
        for (user1 in 0..users.lastIndex){
            if (user==users[user1]){
                users[user1]=newUser
            }
        }
    }

    /**
     * @param user is the user then will be delete
     */
    fun deleteUser(user: User) {
        println("Enter username you want to delete")
        users.remove(user)
    }

    fun changeUser(anotherUser: User,currentUser: User) {
        currentUser
        for (user in 0 until users.lastIndex) {
            if (anotherUser.userName == users[user].userName) {
                currentUser.userName = anotherUser.userName
                return 
            }
        }
    }


    //films
    /**
     * @return a List of all films
     */
    fun getAllFilms(): List<Film> {
        return films
    }

    /**
     * @param newFilm is the film than i will be add
     */
    fun addFilm(newFilm: Film) {
        films.add(newFilm)
    }

    /**
     * @param film is the name of film than i will be delete
     */
    fun deleteFilm(film: Film) {
        println("Enter all fields of film you want delete")
        films.remove(film)
        /*val filmToRemove = Film(title, director, mainActor, genere, length)
        films.remove(filmToRemove)*/
    }

    /**
     * @param currentUser is the user than add films I watched
     * @param film is the film that I watched
     */
    fun watchFilms(currentUser: User, film: Film) {
        val newWatchFilm = film
        currentUser.viwed.add(newWatchFilm)

        //watched++
    }

    /*fun watchedFilms():List<Film>{

    }*/

    /**
     * @param currentUser is the user than add films to favorite
     * @param film is the film that I add to favorites
     */
    fun addFilmToFavourite(currentUser: User, film: Film) {
        val newFavouriteFilm = film
        currentUser.favorites.add(newFavouriteFilm)
        //favourite++
    }

    fun showFilmFavourite(currentUser: User, film: MutableList<Film>): List<Film> {
        val show = currentUser.favorites
        return show
    }

    //searchui

    /**
     * @param title is the title of the film i will be search
     */
    fun byTitle(title: String)/*:List<Film>*/ {
        //Test ver si son mayusculas o minusculas
        //Test que no encuentre nada o encuentre varios
        println("Enter a title")
        for (film in 0..films.lastIndex) {
            if (title == films[film].title) {
                println(films[film])
            }
        }
        //if () dentro de la lista, una pelicula tiene un campo igual, muestra solo esa lista LISTA DE LISTAS
    }

    /**
     * @param director is the director of the film i will be search
     */
    fun byDirector(director: String) {
        println("Enter a director")
        for (film in 0..films.lastIndex) {
            if (director == films[film].director) {
                println(films[film])
            }
        }
    }

    /**
     * @param actor is the main actor of the film i will be search
     */
    fun byMainActor(actor: String) {
        println("Enter an actor")
        for (film in 0..films.lastIndex) {
            if (actor == films[film].mainActor) {
                println(films[film])
            }
        }
    }

    /**
     * @param genere is the genere of the film i will be search
     */
    fun byGenere(genere: String) {
        println("Enter a genere")
        for (film in 0..films.lastIndex) {
            if (genere == films[film].genere) {
                println(films[film])
            }
        }
    }

    /**
     * @param length is the lenght of the film i will be search
     */
    fun byLength(length: Int) {
        println("Enter a (max) duration")
        for (film in 0..films.lastIndex) {
            if (length == films[film].length) {
                println(films[film])
            }
        }
    }
    fun getRecommendedFilms(): List<Film> {
        return films
    }

}
