package model

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class FilmItbTest{

    @Test
    fun afegirUsuariTest(){
        val filmItb = FilmItb()
        val user = User("mateo")
        filmItb.addUser(user)
        assertEquals(1, filmItb.getAllUsers().size)
    }

    @Test
    fun mostrarUsuari (){
        val filmItb = FilmItb()
        val usertets = User("oriol")
        assertEquals(usertets, filmItb.getUser(User("oriol")))
    }

    @Test
    fun mostrarUsuaris (){
        val filmItb = FilmItb()
        val tam = filmItb.getAllUsers()
        assertEquals(tam, filmItb.getAllUsers())
        /*otra forma
        * val tam = filmItb.getAllUsers().size
        * assertEquals(tam, filmItb.getAllUsers()size)
        * */
    }

    @Test
    fun actualitzarUser(){
        val filmItb = FilmItb()
        val user = User("mateo")
        val upuser = User ("mario")
        filmItb.updateUser(user, upuser)
        val usu = filmItb.getAllUsers()
        assertEquals(usu, filmItb.getAllUsers())
    }

    @Test
    fun borrarUser (){
        val filmItb = FilmItb()
        val user = User("mateo")
        filmItb.deleteUser(user)
        /* otra forma
        val tam = filmItb.getAllUsers()
        assertEquals(tam, filmItb.getAllUsers())*/
        val tam = filmItb.getAllUsers().size
        assertEquals(tam, filmItb.getAllUsers().size)

    }
    //film
    @Test
    fun afegirFilmTest(){
        val filmItb =FilmItb()
        val film = Film("transformers", "stven spilberg", "optimus prime", "accion", 180)
        filmItb.addFilm(film)
        assertEquals(1, filmItb.getAllFilms().size)
    }

    @Test
    fun mostrarFilms (){
        val filmItb = FilmItb()
        val pel = filmItb.getAllFilms()
        assertEquals(pel, filmItb.getAllFilms())
        /*otra forma
        val pel = filmItb.getAllFilms().size
        assertEquals(pel, filmItb.getAllFilms().size)
        * */
    }

    @Test
    fun borrarFilm(){
        val filmItb = FilmItb()
        val film = Film("transformers", "stven spilberg", "optimus prime", "accion", 180)
        filmItb.deleteFilm(film)
        /* otra forma
        val tam = filmItb.getAllFilms()
        assertEquals(tam, filmItb.getAllFilms())*/
        val tam = filmItb.getAllFilms().size
        assertEquals(tam, filmItb.getAllFilms().size)
    }

    @Test
    fun filmVista(){
        val filmItb = FilmItb()
        val user = User("mateo")
        val film = Film("transformers", "stven spilberg", "optimus prime", "accion", 180)
        filmItb.watchFilms(user, film)
        val view = user.viwed
        assertEquals(view, user.viwed)
    }

    @Test
    fun filmShowFavouriteTest(){
        val filmItb = FilmItb()
        val user = User("oriol")
        val film = Film("transformers", "stven spilberg", "optimus prime", "accion", 180)
        filmItb.watchFilms(user, film)
        val view = user.favorites
        assertEquals(view, filmItb.showFilmFavourite(user,user.favorites))
    }

    @Test
    fun filmFavorita(){
        val filmItb = FilmItb()
        val user = User("mateo")
        val film = Film("transformers", "stven spilberg", "optimus prime", "accion", 180)
        filmItb.addFilmToFavourite(user, film)
        val fav = user.favorites
        assertEquals(fav, user.favorites)
    }

    //searchui
    @Test
    fun buscarFilmNom(){
        val filmItb = FilmItb()
        val titulo = "transformers"
        filmItb.byTitle(titulo)
        val film = filmItb.byTitle(titulo)
        assertEquals(film, filmItb.byTitle(titulo))
    }

    @Test
    fun buscarFilmDirector(){
        val filmItb = FilmItb()
        val director = "stven spilberg"
        filmItb.byDirector(director)
        val film = filmItb.byDirector(director)
        assertEquals(film, filmItb.byDirector(director))

    }

    @Test
    fun buscarFilmActor(){
        val filmItb = FilmItb()
        val actor = "optimus prime"
        filmItb.byMainActor(actor)
        val film = filmItb.byMainActor(actor)
        assertEquals(film, filmItb.byMainActor(actor))

    }

    @Test
    fun buscarFilmGenero(){
        val filmItb = FilmItb()
        val genero= "accion"
        filmItb.byGenere(genero)
        val film = filmItb.byGenere(genero)
        assertEquals(film, filmItb.byGenere(genero))

    }

    @Test
    fun buscarFilmDuracion(){
        val filmItb = FilmItb()
        val duracion = 180
        filmItb.byLength(duracion)
        val film = filmItb.byLength(duracion)
        assertEquals(film, filmItb.byLength(duracion))
    }

    @Test
    fun buscarFilmSearch(){
        val filmItb = FilmItb()
            val Recommended = filmItb.getRecommendedFilms()
            assertEquals(Recommended, filmItb.getRecommendedFilms())

    }




}
